#include "syscall.h"
#include "synchop.h"
#define SEMKEY 17

int main(){

	int id = system_SemGet(SEMKEY);
	system_PrintString("Output id is: ");
	system_PrintInt(id);
	system_PrintChar('\n');
	int a = 10;
	int x = system_SemCtl(id, SYNCH_SET, &a);
	system_PrintString("Returned value by synch set: ");
	system_PrintInt(x);
	system_PrintChar('\n');
	int b;
        x = system_SemCtl(id, SYNCH_GET, &b);
	if(x == 0)
	{
		system_PrintString("synch get was successful returned value is: ");
		system_PrintInt(b);
		system_PrintChar('\n');
	}
	system_SemOp(id,1);
        x = system_SemCtl(id, SYNCH_GET, &b);
	if(x == 0)
	{
		system_PrintString("Value after semOP 1 is: ");
		system_PrintInt(b);
		system_PrintChar('\n');
	}
	system_SemOp(id,-1);
        x = system_SemCtl(id, SYNCH_GET, &b);
	if(x == 0)
	{
		system_PrintString("Value after semOP -1 is: ");
		system_PrintInt(b);
		system_PrintChar('\n');
	}
	x = system_SemCtl(id,SYNCH_REMOVE,0);
	if(x == 0)
		system_PrintString("Remove was successful\n");
	return 0;
}
