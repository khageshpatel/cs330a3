#include "syscall.h"


int main(){

	int *A= (int *)system_ShmAllocate(2*sizeof(int));
	A[0] = 1;
	int x = system_Fork();
	if(x==0){
		A[0] = 10;
	}
	else{
		while(A[0]!=10);
		system_PrintString("Shared Memory Changed:\n");
	}
	return 0;
}
