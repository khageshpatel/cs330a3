// addrspace.cc 
//	Routines to manage address spaces (executing user programs).
//
//	In order to run a user program, you must:
//
//	1. link with the -N -T 0 option 
//	2. run coff2noff to convert the object file to Nachos format
//		(Nachos object code format is essentially just a simpler
//		version of the UNIX executable object code format)
//	3. load the NOFF file into the Nachos file system
//		(if you haven't implemented the file system yet, you
//		don't need to do this last step)
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "system.h"
#include "addrspace.h"


//----------------------------------------------------------------------
// SwapHeader
// 	Do little endian to big endian conversion on the bytes in the 
//	object file header, in case the file was generated on a little
//	endian machine, and we're now running on a big endian machine.
//----------------------------------------------------------------------

static void 
SwapHeader (NoffHeader *noffH)
{
	noffH->noffMagic = WordToHost(noffH->noffMagic);
	noffH->code.size = WordToHost(noffH->code.size);
	noffH->code.virtualAddr = WordToHost(noffH->code.virtualAddr);
	noffH->code.inFileAddr = WordToHost(noffH->code.inFileAddr);
	noffH->initData.size = WordToHost(noffH->initData.size);
	noffH->initData.virtualAddr = WordToHost(noffH->initData.virtualAddr);
	noffH->initData.inFileAddr = WordToHost(noffH->initData.inFileAddr);
	noffH->uninitData.size = WordToHost(noffH->uninitData.size);
	noffH->uninitData.virtualAddr = WordToHost(noffH->uninitData.virtualAddr);
	noffH->uninitData.inFileAddr = WordToHost(noffH->uninitData.inFileAddr);
}

//----------------------------------------------------------------------
// AddrSpace::AddrSpace
// 	Create an address space to run a user program.
//	Load the program from a file "executable", and set everything
//	up so that we can start executing user instructions.
//
//	Assumes that the object code file is in NOFF format.
//
//	First, set up the translation from program memory to physical 
//	memory.  For now, this is really simple (1:1), since we are
//	only uniprogramming, and we have a single unsegmented page table
//
//	"executable" is the file containing the object code to load into memory
//----------------------------------------------------------------------

AddrSpace::AddrSpace(OpenFile *executable)
{
    unsigned int i, size;
    unsigned vpn, offset;
    TranslationEntry *entry;
    unsigned int pageFrame;

    executable->ReadAt((char *)&noffH, sizeof(noffH), 0);
    if ((noffH.noffMagic != NOFFMAGIC) && 
		(WordToHost(noffH.noffMagic) == NOFFMAGIC))
    	SwapHeader(&noffH);
    ASSERT(noffH.noffMagic == NOFFMAGIC);

// how big is address space?
    size = noffH.code.size + noffH.initData.size + noffH.uninitData.size 
			+ UserStackSize;	// we need to increase the size
						// to leave room for the stack
    numPages = divRoundUp(size, PageSize);
    size = numPages * PageSize;

    ASSERT(numPages+numPagesAllocated <= NumPhysPages);		// check we're not trying
										// to run anything too big --
										// at least until we have
										// virtual memory

    DEBUG('a', "Initializing address space, num pages %d, size %d\n", 
					numPages, size);
// first, set up the translation 
    pageTable = new TranslationEntry[numPages];
    for (i = 0; i < numPages; i++) {
	pageTable[i].virtualPage = i;
	pageTable[i].physicalPage = -1;
	pageTable[i].valid = FALSE;
	pageTable[i].use = FALSE;
	pageTable[i].dirty = FALSE;
	pageTable[i].readOnly = FALSE;  // if the code segment was entirely on 
					// a separate page, we could set its 
					// pages to be read-only
        pageTable[i].shared = FALSE;
    }
// zero out the entire address space, to zero the unitialized data segment 
// and the stack segment
 

// then, copy in the code and data segments into memory
}

unsigned AddrSpace::allocateSHM(unsigned amount){
       // printf("--> allocateshm called\n");
	unsigned pastPages = numPages;
	unsigned requiredPages = divRoundUp(amount, PageSize);	
	ASSERT(requiredPages + numPagesAllocated <= NumPhysPages);
        numPages = numPages + requiredPages;

        TranslationEntry *newPageTable  = new TranslationEntry[numPages];
	for(int i=0;i<pastPages;i++){
		newPageTable[i].virtualPage = pageTable[i].virtualPage;
		newPageTable[i].physicalPage = pageTable[i].physicalPage;
		newPageTable[i].valid = pageTable[i].valid;
		newPageTable[i].use  = pageTable[i].use;
		newPageTable[i].dirty = pageTable[i].dirty;
		newPageTable[i].readOnly = pageTable[i].readOnly;
		newPageTable[i].shared = pageTable[i].shared;
	}	
        
        for(int i=pastPages;i<numPages;++i){
		newPageTable[i].virtualPage = i;
		int *freePageNo = (int *)freePages->Remove();
		if(freePageNo != NULL)
			newPageTable[i].physicalPage = *freePageNo;
		else{
			newPageTable[i].physicalPage = indOfUnAllocatedPage;
			indOfUnAllocatedPage++;
		}
		newPageTable[i].valid = TRUE;
		newPageTable[i].use = FALSE;
		newPageTable[i].dirty = FALSE;
		newPageTable[i].shared = TRUE;
	}
	
	stats->updatePageFaults(requiredPages);
		
	unsigned startAddr;
	for(int i=pastPages;i<numPages;i++){
		startAddr = newPageTable[i].physicalPage*PageSize;
		bzero(&machine->mainMemory[startAddr],PageSize);
	}
	
	numPagesAllocated = numPagesAllocated + requiredPages;

	delete pageTable;
	pageTable = newPageTable;
	machine->pageTable = pageTable;
	machine->pageTableSize = numPages*PageSize;
      //  printf("--> Allocate shm returning: %d\n",startAddr);
	return pastPages*PageSize;
	
}

//----------------------------------------------------------------------
// AddrSpace::AddrSpace (AddrSpace*) is called by a forked thread.
//      We need to duplicate the address space of the parent.
//----------------------------------------------------------------------

AddrSpace::AddrSpace(AddrSpace *parentSpace)
{
    numPages = parentSpace->GetNumPages();
    noffH = parentSpace->noffH;
    strcpy(filePath, parentSpace->filePath);
    unsigned i,k;
    unsigned countNumSharedPages = 0;
    TranslationEntry *parentPageTable = parentSpace->GetPageTable();
    for(i=0;i<numPages;i++)
	if(parentPageTable[i].shared == TRUE)
		countNumSharedPages++;
    unsigned pagesToAllocate = numPages - countNumSharedPages;
    unsigned size = numPages*PageSize;
    ASSERT(pagesToAllocate+numPagesAllocated <= NumPhysPages);                // 
    DEBUG('a', "Initializing address space, num pages %d, size %d\n",
                                        numPages, size);
    // first, set up the translation
    pageTable = new TranslationEntry[numPages];
    for (i = 0,k=0; i < numPages; i++) {
        pageTable[i].virtualPage = i;
	pageTable[i].physicalPage = -1;
        pageTable[i].valid = parentPageTable[i].valid;
        pageTable[i].use = parentPageTable[i].use;
        pageTable[i].dirty = parentPageTable[i].dirty;
        pageTable[i].readOnly = parentPageTable[i].readOnly;
       pageTable[i].shared = parentPageTable[i].shared;
    }
    
    unsigned startAddrParent;
    unsigned startAddrChild;
    for (i=0; i<numPages; i++) {
       if(pageTable[i].shared==TRUE)
		pageTable[i].physicalPage = parentPageTable[i].physicalPage;
	else if(pageTable[i].valid == TRUE)
	{
		int *freePageNo = (int *)freePages->Remove();
		if(freePageNo!=NULL)
			pageTable[i].physicalPage = *freePageNo;
		else{
			pageTable[i].physicalPage = indOfUnAllocatedPage;
			indOfUnAllocatedPage++;
		}
	   startAddrParent = parentPageTable[i].physicalPage*PageSize;
	   startAddrChild  = pageTable[i].physicalPage*PageSize;
	   for(k=0;k<PageSize;k++) 
       	   	machine->mainMemory[startAddrChild+k] = machine->mainMemory[startAddrParent+k];
	   stats->updatePageFaults(1);
	   currentThread->SortedInsertInWaitQueue(1000+stats->totalTicks);
	}
       
    }

    numPagesAllocated += pagesToAllocate;
}

//----------------------------------------------------------------------
// AddrSpace::~AddrSpace
// 	Dealloate an address space.  Nothing for now!
//----------------------------------------------------------------------

AddrSpace::~AddrSpace()
{
   delete pageTable;
}

//----------------------------------------------------------------------
// AddrSpace::InitRegisters
// 	Set the initial values for the user-level register set.
//
// 	We write these directly into the "machine" registers, so
//	that we can immediately jump to user code.  Note that these
//	will be saved/restored into the currentThread->userRegisters
//	when this thread is context switched out.
//----------------------------------------------------------------------

void
AddrSpace::InitRegisters()
{
    int i;

    for (i = 0; i < NumTotalRegs; i++)
	machine->WriteRegister(i, 0);

    // Initial program counter -- must be location of "Start"
    machine->WriteRegister(PCReg, 0);	

    // Need to also tell MIPS where next instruction is, because
    // of branch delay possibility
    machine->WriteRegister(NextPCReg, 4);

   // Set the stack register to the end of the address space, where we
   // allocated the stack; but subtract off a bit, to make sure we don't
   // accidentally reference off the end!
    machine->WriteRegister(StackReg, numPages * PageSize - 16);
    DEBUG('a', "Initializing stack register to %d\n", numPages * PageSize - 16);
}

//----------------------------------------------------------------------
// AddrSpace::SaveState
// 	On a context switch, save any machine state, specific
//	to this address space, that needs saving.
//
//	For now, nothing!
//----------------------------------------------------------------------

void AddrSpace::SaveState() 
{}

//----------------------------------------------------------------------
// AddrSpace::RestoreState
// 	On a context switch, restore the machine state so that
//	this address space can run.
//
//      For now, tell the machine where to find the page table.
//----------------------------------------------------------------------

void AddrSpace::RestoreState() 
{
    machine->pageTable = pageTable;
    machine->pageTableSize = numPages;
}

unsigned
AddrSpace::GetNumPages()
{
   return numPages;
}

TranslationEntry*
AddrSpace::GetPageTable()
{
   return pageTable;
}
