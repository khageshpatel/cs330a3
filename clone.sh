cd ~/cs330assignment1/nachos/code/

# Clone just the repository's .git folder (excluding files as they are already in
# existing directory) into an empty temporary directory
git clone --no-checkout git@bitbucket.org:amitmunje/cs330.git temp.tmp # might want --no-hardlinks for cloning local repo

# Move the .git folder to the directory with the files.
# This makes existing directory a git repo.
mv temp.tmp/.git ./

# Delete the temporary directory
rmdir temp.tmp

# git thinks all files are deleted, this reverts the state of the repo to HEAD.
# WARNING: any local changes to the files will be lost.
git reset --hard HEAD
